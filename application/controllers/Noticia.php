<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Noticia extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Option_model', 'option');
        $this->load->model('Noticia_model', 'noticia');
    }

    public function index()
    {
        redirect('noticia/listar', 'refresh');
    }

    public function listar()
    {
        //verifica login se o usuário está logado
        verifica_login();

        //carrega view
        $dados['titulo'] = 'Igor Matheus - Listagem de Notícias';
        $dados['h2'] = 'Listagem de Notícias';
        $dados['tela'] = 'listar';
        $dados['noticias'] = $this->noticia->get();
        $this->load->view('painel/noticias', $dados);
    }

    public function cadastrar()
    {
        //verifica login se o usuário está logado
        verifica_login();

        //regras de validação
        $this->form_validation->set_rules('titulo', 'TÍTULO', 'trim|required');
        $this->form_validation->set_rules('conteudo', 'CONTEÚDO', 'trim|required');

        //Verifica a Validação
        if ($this->form_validation->run() == FALSE) :
            if (validation_errors()) :
                set_msg(validation_errors());
            endif;
        else :
            $this->load->library('upload', config_upload());
            if ($this->upload->do_upload('imagem')) :
                // Upload foi efetuado
                $dados_upload = $this->upload->data();
                $dados_form = $this->input->post();
                $dados_insert['titulo'] = to_db($dados_form['titulo']);
                $dados_insert['conteudo'] = to_db($dados_form['conteudo']);
                $dados_insert['imagem'] = $dados_upload['file_name'];
                // Salvar no banco de dados
                if ($id = $this->noticia->salvar($dados_insert)) :
                    set_msg('<p>Notícia cadastrada com sucesso!</p>');
                    redirect('noticia/listar', 'refresh');
                else :
                    set_msg('<p>Erro! Notícia não cadastrada.</p>');
                endif;

            else :
                // erro no upload
                $msg = $this->upload->display_errors();
                $msg .= '<p>São permitidos arquivos JPG e PNG de até 512kb. </p>';
                set_msg($msg);
            endif;
        endif;
        //carrega view
        $dados['titulo'] = 'Igor Matheus - Cadastro de Notícias';
        $dados['h2'] = 'Cadastro de notícias';
        $dados['tela'] = 'cadastrar';
        $this->load->view('painel/noticias', $dados);
    }

    public function excluir()
    {

        //verifica login se o usuário está logado
        verifica_login();

        $id = $this->uri->segment(3);
        if ($id > 0) :
            // id informado, continuar com exclusão
            if ($noticia = $this->noticia->get_single($id)) :
                $dados['noticia'] = $noticia;

            else :
                set_msg('<p>Notícia inexistente! Escolha uma notícia para excluir!</p>');
                redirect('noticia/listar', 'refresh');
            endif;
        else :
            set_msg('<p>Você de escolher uma notícia para excluir!</p>');
            redirect('noticia/listar', 'refresh');
        endif;


        // Regras de validação
        $this->form_validation->set_rules('enviar', 'ENVIAR', 'trim|required');

        // verifica validação
        if ($this->form_validation->run() == FALSE) :
            if (validation_errors()) :
                set_msg(validation_errors());
            endif;
        else :
            $imagem = 'uploads/'.$noticia->imagem;
            if($this->noticia->excluir($id)):
                unlink($imagem);
                set_msg('<p>Notícia exluída com sucesso!</p>');
                redirect('noticia/listar', 'reflesh');
            else :
                set_msg('<p>Erro! Nenhuma notícia foi excluída.</p>');
            endif;
        endif;

        //carrega view
        $dados['titulo'] = 'Igor Matheus - Exclusão de Notícias';
        $dados['h2'] = 'Excluir de notícias';
        $dados['tela'] = 'excluir';
        $this->load->view('painel/noticias', $dados);
    }
}
